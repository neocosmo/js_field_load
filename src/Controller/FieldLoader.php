<?php

namespace Drupal\js_field_load\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller for field load path.
 */
class FieldLoader extends ControllerBase {

  /**
   * Cache service object.
   *
   * @var cache
   */
  protected $renderCache;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('cache.default')
    );
  }

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $render_cache
   *   A render cache object.
   */
  public function __construct(CacheBackendInterface $render_cache) {
    $this->renderCache = $render_cache;
  }

  /**
   * Callback that loads a rendered field from cache.
   *
   * @param string $field_cid
   *   The cache ID of the field.
   * @param string $selector
   *   The selector string for the HTML element which is being replaced by the
   *   returned data.
   *
   * @return string
   *   HTML of either the rendered field or an error message.
   */
  public function content(string $field_cid, string $selector) {
    $cached = $this->renderCache->get($field_cid);
    $html = [
      '#theme' => 'js_field_load_error',
      '#title' => $this->t('Error'),
      '#content' => $this->t('An error occured loading the content. Please reload the page.'),
    ];
    if ($cached) {
      $html = $cached->data;
    }
    $cmd = new ReplaceCommand($selector, $html);
    $response = new AjaxResponse();
    $response->addCommand($cmd);
    return $response;
  }

}
