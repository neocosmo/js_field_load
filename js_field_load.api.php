<?php

/**
 * @file
 * Hooks specific to the JS Field Load module.
 */

/**
 * Decide whether a field should be loaded via JS or not.
 *
 * @param string $field_name
 *   The machine name of the field.
 * @param string $field_type
 *   The type of the field.
 * @param string $view_mode
 *   The view mode of the entity as a part of which the field is rendered.
 * @param string $entity_type
 *   The type of the entity, to which the field belong.
 * @param string $bundle
 *   The bundle of the entity the field belongs to.
 *
 * @return bool
 *   Return TRUE if the field should be loaded via JS or FALSE if the field
 *   should be loaded normally. The results of all hook implementations are or'd
 *   together, so only one implementation needs to return TRUE for the field
 *   being loaded via JS.
 */
function hook_js_field_load_enabled(
  $field_name,
  $field_type,
  $view_mode,
  $entity_type,
  $bundle) {

  return $field_name === 'field_video' && $view_mode === 'full'
    && $entity_type === 'node';
}
