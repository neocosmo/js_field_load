<?php

namespace Drupal\js_field_load\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ThemeHandler;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configuration settings form for js_field_load.
 */
class Settings extends ConfigFormBase {

  const SETTINGS = 'js_field_load.settings';

  /**
   * Theme handler.
   *
   * @var themeHandler
   */
  protected $themeHandler;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('theme_handler')
    );
  }

  /**
   * Constructor of Settings.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Extension\ThemeHandler $theme_handler
   *   The theme handler.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    ThemeHandler $theme_handler) {

    parent::__construct($config_factory);
    $this->themeHandler = $theme_handler;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'js_field_load_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $theme_options = [];
    foreach ($this->themeHandler->listInfo() as $name => $info) {
      if (!empty($info->info['hidden'])) {
        continue;
      }
      $theme_options[$name] = $info->info['name'];
    }
    $config = $this->config(static::SETTINGS);
    $form['themes'] = [
      '#type' => 'select',
      '#title' => $this->t('Themes'),
      '#description' => $this->t('Select the themes for which js_field_load is enabled.'),
      '#multiple' => TRUE,
      '#options' => $theme_options,
      '#default_value' => $config->get('themes', []),
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   *
   * @todo Clear cache after changing the settings. When the themes are changed,
   * the change will only be visible after a cache flush.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);
    $config->set('themes', $form_state->getValue('themes'));
    $config->save();
    return parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

}
